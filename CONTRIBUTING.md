CONTRIBUTING
------

This documentation is for contributors and maintainers.

### What coding environment development Ukeep support ?
You need :
- `nodeJS` (>= 10.3)
- `npm` (>= 6.2)
- `git`

Code syntax is `ES2018`, you can view new features [here](https://medium.com/front-end-weekly/javascript-whats-new-in-ecmascript-2018-es2018-17ede97f36d5)    
A linter `linter name` has been configured, you can find the configuration [here](LINK TO LINTER CONF)

BEFORE coding you need to create a new branch.   
```
git checkout -b ${BRANCH_NAME}
```

When your feature seems good, create a commit
### How to commit ?
Do not use `-m` option
```
git commit
```

Git commit are formated like following:
```
Commit message title

Commit description, line 1
line 2, 
line 3
```
Exit editor then push.

After that, you need to made a push request, go to ukeep project page,    
Choose your branch, then click on merge request.