const http = require('http');
const express = require('express');
const path = require('path');
const compress = require('compression');

const express_object = express();

// Compress files for better loading benchs
express_object.use(compress());

// Serve static assets normally
express_object.use(express.static(path.join((__dirname + '/dist'))));

express_object.get('*', function(req, res) {
  res.sendFile(path.join(__dirname + '/dist/index.html'));
});

http.createServer(express_object).listen(8080);
