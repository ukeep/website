import {
  Directive,
  Output,
  OnInit,
  HostListener,
  AfterViewInit,
  EventEmitter,
} from '@angular/core';

import {
  Subject,
} from 'rxjs';

import {
  throttleTime
} from 'rxjs/operators';


@Directive({ selector: '[appMouseWheel]' })
export class MouseWheelDirective implements AfterViewInit {
  @Output() mouseWheelUp = new EventEmitter();
  @Output() mouseWheelDown = new EventEmitter();

  private mouse = new Subject<any>();
  private mouseObs = this.mouse.asObservable();

  @HostListener('wheel', ['$event']) onMouseWheel(event: any) {
    console.log('debounce');
    event.preventDefault();
    event.stopImmediatePropagation();
    this.mouse.next(event);
  }

  /**
   * What get executed at comopnent launch
   */
  public ngAfterViewInit() {
    this.mouseObs.pipe(throttleTime(1000)).subscribe(x => this.mouseWheelFunc(x));
  }

  /**
   * Handle mouse wheel event
   */
  public mouseWheelFunc(event: any) {
    // const event = window.event || event; // old IE support
    const delta = Math.max(-1, Math.min(1, (-event.deltaY || -event.detail)));

    if (delta > 0) {
      this.mouseWheelUp.emit(event);
    } else if (delta < 0) {
      this.mouseWheelDown.emit(event);
    }

    if (event.preventDefault) {
      // event.preventDefault();
      // event.stopPropagation();
    }
  }
}
