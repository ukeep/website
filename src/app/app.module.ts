// Angular dependencies
import {
  NgModule
} from '@angular/core';

import {
  FormsModule
} from '@angular/forms';

import {
  BrowserModule
} from '@angular/platform-browser';

// App starting points
import {
  AppComponent
} from './app.component';

import {
  AppRoutingModule
} from './app-routing.module';

// Custom dependencies
import {
  MouseWheelDirective
} from './directives/mouse-wheel.directive';

import {
  ContactFormComponent
} from './components/contact-form/contact-form.component';

import {
  FontAwesomeModule,
} from '@fortawesome/angular-fontawesome';

import {
  library,
} from '@fortawesome/fontawesome-svg-core';

import {
  fas,
} from '@fortawesome/free-solid-svg-icons';

import {
  far,
} from '@fortawesome/free-regular-svg-icons';

// Add icons into the library
library.add(fas, far);


/**
 * Handle special configuration for Hammer to enable mobile vertical swipes
 */
import * as Hammer from 'hammerjs';
import {
  HammerGestureConfig,
  HAMMER_GESTURE_CONFIG
} from '@angular/platform-browser';

export class HammerConfig extends HammerGestureConfig {
  overrides = {
    swipe: { direction: Hammer.DIRECTION_ALL }
  };
}

@NgModule({
  declarations: [
    AppComponent,
    ContactFormComponent,
    MouseWheelDirective
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
  ],
  providers: [{
    provide: HAMMER_GESTURE_CONFIG,
    useClass: HammerConfig
  }],
  bootstrap: [AppComponent],
})
export class AppModule { }
