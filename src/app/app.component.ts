// Angular dependencies
import {
  Component,
  OnInit,
  QueryList,
  ElementRef,
  ViewChildren,
} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

/**
 * The global app component
 */
export class AppComponent implements OnInit {
  /**
   * Holds pages elements list
   */
  @ViewChildren('element') element: QueryList<ElementRef>;

  /**
   * Holds pages configuration
   */
  public $pagesConfiguration: any;

  /**
   * Holds the position parameter
   */
  public $currentPos = 0;

  /**
   * Holds the smoth scroll configuration
   */
  protected smooth = {
    behavior: 'smooth',
    block: 'start',
    inline: 'nearest'
  };


  /**
   * Constructor
   */
  constructor() { }

  /**
   * What get executed when component get load
   */
  public ngOnInit() {
    // Always start at page top
    window.scrollTo(0, 0);

    // Set pages configurations
    this.$pagesConfiguration = [{
      pageName: 'Intro',
      pageBg: `url('/assets/images/background.png')`,
      type: 'intro'
    }, {
      pageName: 'Resume',
      pageBg: '#d4defd',
      type: 'resume'
    }, {
      pageName: 'Contact',
      pageBg: '#dfe3fd',
      type: 'contact',
      center: 'true',
      content: 'Stay tuned'
    }];
  }

  /**
   * Function to get back on top
   */
  public $backToTop() {
    const el: ElementRef[] = this.element.toArray();
    el[0].nativeElement.scrollIntoView(this.smooth);
    this.$currentPos = 0;
  }

  /**
   * Function to scroll to the page pointe by direction
   */
  protected handleScroll(direction: any) {
    // Get pages elements
    const el: ElementRef[] = this.element.toArray();

    // Set index to current position
    let index = this.$currentPos;

    // Define the index move by direction
    direction === true ? index-- : index++;

    // If the pointed element exist, do the scroll to
    if (el[index]) {
      this.$currentPos = index;

      el[index].nativeElement.scrollIntoView(this.smooth);
    }
  }

  /**
   * Mouse wheel up event trigger
   */
  public mouseWheelUpFunc() {
    console.log('wheel');
    this.handleScroll(true);
  }

  /**
   * Mouse wheel down event trigger
   */
  public mouseWheelDownFunc() {
    console.log('wheel');
    this.handleScroll(false);
  }
}
