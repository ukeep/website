import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {

  /**
   * Holds the user name
   */
  public $userName;

  /**
   * Holds the user email
   */
  public $userMail;

  /**
   * Holds the user comment
   */
  public $userComment;


  constructor() { }

  public ngOnInit() {

  }

}
